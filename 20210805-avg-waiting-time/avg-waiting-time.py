#!/usr/bin/env python
'''
Well, so the solution came out on Friday afternoon on 20210806.
Turns out the heap datastructure is needed.
I am going to study Chapter 6 in the algorithm textbook.
'''

import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--max_time', type=int, default=10)
    parser.add_argument('-n', '--num_custs', type=int, default=5, help='Number of generated customers.')
    parser.add_argument('-p', '--rand_prep_time', type=int, default=5, help='Maximum of the random prepration time.')
    args = parser.parse_args()
    print(type(args))
